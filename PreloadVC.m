//
//  PreloadVC.m
//  DatasObjectiveCApp
//
//  Created by Aleksandr Afanasiev on 04.09.17.
//  Copyright © 2017 Aleksandr Afanasiev. All rights reserved.
//

#import "PreloadVC.h"
#import "TipsView.h"

@interface PreloadVC()

@end

@implementation PreloadVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupScrollView];

}

-(void) setupScrollView
{
    self.tipScrollView.contentSize = CGSizeMake(self.view.frame.size.width * 3, self.tipScrollView.frame.size.height);
    _tipScrollView.delegate = self;
    
    for (int i = 0; i < 3; i++) {

        TipsView *tipsView = [[TipsView alloc] initWithFrame:CGRectMake(self.view.frame.size.width * i, 0, self.view.frame.size.width, self.tipScrollView.frame.size.height)];
        tipsView.tipsLabel.text = [NSString stringWithFormat: @"Текст типсы %d", i];
        tipsView.tipsTextView.text = @"Lorem ipsum dolor sit er elit lamet, consectetaur cillium adipisicing pecu, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
        [self.tipScrollView addSubview: tipsView];
        
        
    }
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    UIColor *green = [UIColor colorWithRed:57.0/255.0 green:156.0/255.0 blue:52.0/255.0 alpha:1.0];
    UIColor *gray = [UIColor lightGrayColor];

    int position =(scrollView.contentOffset.x / self.view.frame.size.width);
    switch (position) {
        case 0:
            self.itemIndicator1.backgroundColor = green;
            self.itemIndicator2.backgroundColor = gray;
            self.itemindicator3.backgroundColor = gray;
            break;
        case 1:
            self.itemIndicator1.backgroundColor = gray;
            self.itemIndicator2.backgroundColor = green;
            self.itemindicator3.backgroundColor = gray;
            break;
        default:
            self.itemIndicator1.backgroundColor = gray;
            self.itemIndicator2.backgroundColor = gray;
            self.itemindicator3.backgroundColor = green;
            break;
    }
}


- (IBAction)continueAction:(UIButton *)sender
{
    [UIView animateWithDuration:0.2 animations:^{
        self.preview.hidden = YES;
        self.viewLoading.hidden = NO;
        [self.activityIndicator startAnimating];
    } completion:^(BOOL finished) {
        [self durationOfAnimation:4];
    }];
}

- (void) durationOfAnimation: (double) seconds
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((DISPATCH_TIME_NOW + seconds) * NSEC_PER_SEC )), dispatch_get_main_queue(), ^{
        
        [self.activityIndicator stopAnimating];
        self.activityIndicator.hidden = YES;
        self.viewLoading.hidden = YES;
        self.loginView.hidden = NO;
    });
}



- (IBAction)loginAction:(UIButton *)sender {
}
@end









































































