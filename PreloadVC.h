//
//  PreloadVC.h
//  DatasObjectiveCApp
//
//  Created by Aleksandr Afanasiev on 04.09.17.
//  Copyright © 2017 Aleksandr Afanasiev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TipsView.h"

@interface PreloadVC : UIViewController<UIScrollViewDelegate>


@property (weak, nonatomic) IBOutlet UIView *preview;
@property (weak, nonatomic) IBOutlet UIView *viewLoading;
@property (weak, nonatomic) IBOutlet UIView *loginView;

@property (weak, nonatomic) IBOutlet UIScrollView *tipScrollView;
@property (weak, nonatomic) IBOutlet UIView *itemIndicator1;
@property (weak, nonatomic) IBOutlet UIView *itemIndicator2;
@property (weak, nonatomic) IBOutlet UIView *itemindicator3;

- (IBAction)continueAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

- (IBAction)loginAction:(UIButton *)sender;

//@property(strong, nonatomic) TipsView *tipsView;


@end
