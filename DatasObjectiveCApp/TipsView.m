//
//  TipsView.m
//  DatasObjectiveCApp
//
//  Created by Aleksandr Afanasiev on 04.09.17.
//  Copyright © 2017 Aleksandr Afanasiev. All rights reserved.
//

#import "TipsView.h"

@implementation TipsView

@synthesize tipsLabel = _tipsLabel;
@synthesize tipsTextView = _tipsTextView;


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self) {
        _tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, self.frame.size.width - 20, 25)];
        _tipsLabel.textAlignment = NSTextAlignmentCenter;
        [_tipsLabel setFont:[UIFont systemFontOfSize:24]];
        _tipsLabel.textColor = [UIColor blackColor];
        [self addSubview:_tipsLabel];
        
        _tipsTextView = [[UITextView alloc] initWithFrame:CGRectMake(20, 55, self.frame.size.width - 40, self.frame.size.height - 45)];
        _tipsTextView.textAlignment = NSTextAlignmentCenter;
        [_tipsTextView setFont:[UIFont systemFontOfSize:18]];
        [self addSubview:_tipsTextView];

    }
    
    return self;
}

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    return self;
}


@end














































