//
//  main.m
//  DatasObjectiveCApp
//
//  Created by Aleksandr Afanasiev on 04.09.17.
//  Copyright © 2017 Aleksandr Afanasiev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
