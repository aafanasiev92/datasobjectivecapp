//
//  TipsView.h
//  DatasObjectiveCApp
//
//  Created by Aleksandr Afanasiev on 04.09.17.
//  Copyright © 2017 Aleksandr Afanasiev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipsView : UIView

    @property (strong, nonatomic) UILabel *tipsLabel;
    @property (strong, nonatomic) UITextView *tipsTextView;
    
@end
